DEST = dest

SNX_OUTPUTS = html,gopher,gemini
SNX_PREFIX_FLAGS = -p html:https://lunacb.house \
	-p gemini:gemini://lunacb.house \
	-p gopher:gopher://lunacb.house

SNX_DEST = -d ${DEST}
SNX_FLAGS = -o ${SNX_OUTPUTS} ${SNX_PREFIX_FLAGS} ${SNX_DEST}

all: site

site: main blog projects assets

main:
	snx ${SNX_FLAGS} -t document -s main/src -x main/templates

blog:
	snx ${SNX_FLAGS} -t document,index,tag_index -s blog/src \
		-x blog/templates --tag-dir=blog/tags \
		--tag-index-file=blog/tags/index \
		--index-file=blog/index

blog_rss:
	snx -o rss ${SNX_PREFIX_FLAGS} -d rss:${DEST}/html -t index -s blog/src \
		-x blog/templates --tag-dir=blog/tags \
		--tag-index-file=blog/tags/index \
		--index-file=blog/feed

projects:
	snx ${SNX_FLAGS} -t index -s projects/src \
		-x projects/templates --index-file=projects

assets:
	install -m 644 -D assets/main.css ${DEST}/html/assets/css/main.css

clean:
	rm -rf ${DEST}/*

.PHONY: all site main blog projects assets

